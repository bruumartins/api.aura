﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MeuTCC.Domain.Entities
{
    public class Expense : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public double Value { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int StatusId { get; set; }
        public virtual Status Status { get; set; }
        public virtual List<Expense_Product> Products { get; set; }

    }
}
