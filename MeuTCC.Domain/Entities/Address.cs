﻿namespace MeuTCC.Domain.Entities
{
    public class Address : BaseEntity
    {
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Neighborhood { get; set; }
        public int Number { get; set; }
        public string CEP { get; set; }
        public string Country { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public int? ClientId { get; set; }
        public virtual Client Client { get; set; }
        public int? ProviderId { get; set; }
        public virtual Provider Provider { get; set; }
    }
}
