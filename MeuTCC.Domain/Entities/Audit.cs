﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MeuTCC.Domain.Entities
{
    public class Audit:BaseEntity
    {
        public int ItemId { get; set; }
        public string Domain { get; set; }
        public string Action { get; set; }
        public string Json { get; set; }

        public Audit()
        {
            Json = string.Empty;
        }
    }
}
