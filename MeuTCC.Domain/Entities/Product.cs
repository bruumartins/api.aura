﻿using System.Collections.Generic;

namespace MeuTCC.Domain.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public double SaleValue { get; set; }
        public double PurchaseValue { get; set; }
        public int Quantity { get; set; }
        public string Code { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual List<Sale_Product> Sales { get; set; }
        public virtual List<Expense_Product> Expenses { get; set; }
        public virtual List<Provider_Product> Providers { get; set; }
    }
}
