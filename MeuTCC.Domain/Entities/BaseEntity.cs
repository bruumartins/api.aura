﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MeuTCC.Domain.Entities
{
    public abstract class BaseEntity
    {
        [Required]
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public string ObjectName
        {
            get
            {
                return this.GetType().Name;
            }
        }
        public IEnumerable<ValidationResult> Validate()
        {
            var result = new List<ValidationResult>();
            var context = new ValidationContext(this, null, null);
            Validator.TryValidateObject(this, context, result, true);
            return result;
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public JObject ToObject()
        {
            return JObject.FromObject(this);
        }
    }
}
