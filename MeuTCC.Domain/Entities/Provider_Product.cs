﻿using System.Text.Json.Serialization;

namespace MeuTCC.Domain.Entities
{
    public class Provider_Product : BaseEntity
    {
        public int ProviderId { get; set; }
        public virtual Provider Provider { get; set; } 
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
