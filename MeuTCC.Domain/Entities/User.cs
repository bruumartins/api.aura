﻿using System.Collections.Generic;

namespace MeuTCC.Domain.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            Type = UserType.User;
        }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Document { get; set; }
        public string Password { get; set; }
        public string GoogleGuid { get; set; }
        public UserType Type { get; set; }
        public virtual List<Client> Clients { get; set; }
        public virtual List<Expense> Expenses { get; set; }
        public virtual List<Sale> Sales { get; set; }
        public virtual List<Reminder> Reminders { get; set; }
        public virtual Address Address { get; set; }
        public virtual List<Provider> Providers { get; set; }
        public virtual List<Product> Products { get; set; }
    }
}