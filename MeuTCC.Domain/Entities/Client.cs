﻿using System.ComponentModel.DataAnnotations;

namespace MeuTCC.Domain.Entities
{
    public class Client : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        public string Document { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public virtual Address Address { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
