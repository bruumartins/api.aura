﻿using System.Collections.Generic;

namespace MeuTCC.Domain.Entities
{
    public class Provider : BaseEntity
    {
        public string Name { get; set; }
        public string Document { get; set; }
        public string Phone { get; set; }
        public virtual Address Address { get; set; }
        public virtual List<Provider_Product> Products { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
