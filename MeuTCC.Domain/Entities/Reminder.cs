﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MeuTCC.Domain.Entities
{
    public class Reminder : BaseEntity
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Description { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
    }
}
