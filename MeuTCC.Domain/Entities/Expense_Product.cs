﻿namespace MeuTCC.Domain.Entities
{
    public class Expense_Product : BaseEntity
    {
        public int ExpenseId { get; set; }
        public virtual Expense Expense { get; set; } 
        public int ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}
