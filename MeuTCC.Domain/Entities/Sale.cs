﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeuTCC.Domain.Entities
{
    public class Sale : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public double Value { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public int? ClientId { get; set; }
        public virtual Client Client { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int StatusId { get; set; }
        public virtual Status Status { get; set; }
        public virtual List<Sale_Product> Products { get; set; } 

    }
}
