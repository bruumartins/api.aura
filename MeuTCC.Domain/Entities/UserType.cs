﻿namespace MeuTCC.Domain.Entities
{
    public enum UserType
    {        
        Admin = 1,
        User = 2
    }
}
