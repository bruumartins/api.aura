﻿using System;

namespace MeuTCC.Domain.Utils
{
    public class PaginationData
    {
        /// <summary>
        /// Quantidade total de itens
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Número da página atual
        /// </summary>
        public int CurrentPage { get; set; }

        /// <summary>
        /// Quantidade de itens por página
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Número total de páginas
        /// </summary>
        public int TotalPages => (int)Math.Ceiling(decimal.Divide(Count, PageSize));
    }
}
