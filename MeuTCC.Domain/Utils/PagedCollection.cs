﻿using System.Collections.Generic;

namespace MeuTCC.Domain.Utils
{
    public class PagedCollection<T> where T : class
    {
        /// <summary>
        /// Objeto de retorno da paginação
        /// </summary>
        public IEnumerable<T> Items { get; set; }

        /// <summary>
        /// Dados da paginação
        /// </summary>
        public PaginationData PaginationData { get; set; }
    }
}
