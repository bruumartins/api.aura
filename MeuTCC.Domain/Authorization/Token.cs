﻿using System.Net;

namespace MeuTCC.Domain.Authorization
{
    public class Token
    {
        public string token { get; set; }
        public string refreshToken { get; set; }
        public string userType { get; set; }
        public string message { get; set; }
        public HttpStatusCode status { get; set; }
    }
}
