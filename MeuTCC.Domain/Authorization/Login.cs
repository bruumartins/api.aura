﻿namespace MeuTCC.Domain.Authorization
{
    public class Login
    {
        /// <summary>
        /// Nome de usuário
        /// </summary>
        public string userEmail { get; set; }

        /// <summary>
        /// Chave de acesso, podendo ser a senha do usuário, token do Google ou RefreshToken
        /// </summary>
        public string accessKey { get; set; }

        /// <summary>
        /// Tipo de acesso: password, refresh_token, google, facebook
        /// </summary>
        public string grantType { get; set; }
    }
}
