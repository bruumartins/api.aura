﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Domain.Authorization
{
    public class Credentials
    {
        public string ValidationUrl { get; set; }
    }

    public class GoogleCredentials : Credentials
    {
        public string[] ClientId { get; set; }
    }
}
