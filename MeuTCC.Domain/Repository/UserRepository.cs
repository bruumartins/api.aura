﻿using MeuTCC.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace MeuTCC.Domain.Repository
{
    public static class UserRepository
    {
        public static User Get(string username, string password)
        {
            var users = new List<User>();
            users.Add(new User { Id = 1, Name = "batman", Password = "batman", Email = "batman@example.com", Document = "12121212112" });
            users.Add(new User { Id = 2, Name = "robin", Password = "robin", Email = "robin@example.com", Document = "12121212113" });
            return users.Where(x => x.Name.ToLower() == username.ToLower() && x.Password == x.Password).FirstOrDefault();
        }
    }
}
