﻿using MeuTCC.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MeuTCC.Domain.Intarfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        void Insert(T obj);

        void Update(T obj);

        void Delete(int id);

        //T Find(int value);
        T Find(int value, IQueryable<T> queryable = null);

        //T Single(List<Expression<Func<T, bool>>> Expressions = null);
        T Single(List<Expression<Func<T, bool>>> Expressions = null, IQueryable<T> queryable = null);

        //IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null);
        IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null, IQueryable<T> queryable = null);

        //IList<T> PagedSelect(List<Expression<Func<T, bool>>> Expressions = null, int page = 1, int pageSize = 10);
        IList<T> PagedSelect(List<Expression<Func<T, bool>>> Expressions = null, IQueryable<T> queryable = null, int page = 1, int pageSize = 10);

        int Count(List<Expression<Func<T, bool>>> Expression = null);
    }
}
