﻿using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MeuTCC.Domain.Intarfaces
{
    public interface IService<T> where T : BaseEntity
    {
        T Insert(T obj);

        T Update(T obj);

        void Delete(int id);

        IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null);

        T Find(int value);

        PagedCollection<T> PagedSelect(List<Expression<Func<T, bool>>> Expression = null, int page = 1, int pageSize = 10);
    }
}
