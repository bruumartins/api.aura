﻿using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MeuTCC.Service
{
    public class ProductService<T> : BaseService<T> where T : Product
    {
        ProviderProductService<Provider_Product> _providerProductService;
        ProductRepository<Product> _productRepository;
        public ProductService()
        {
            _providerProductService = new ProviderProductService<Provider_Product>();
            _productRepository = new ProductRepository<Product>();
        }

        public Product Insert(T obj)
        {
            var providers = obj.Providers;
            obj.Providers = null;
            Product product = base.Insert(obj);

            if(providers != null && providers.Count > 0) 
            {
                providers.ForEach(element => {
                    Provider_Product providerProduct = new Provider_Product();
                    providerProduct.ProductId = product.Id;
                    providerProduct.ProviderId = element.ProviderId;

                    _providerProductService.Insert(providerProduct);
                });
            }

            return product;
        }

        public Product Update(T obj)
        {
            var providers = obj.Providers;
            obj.Providers = null;
            Product product = base.Update(obj);

            if (providers != null && providers.Count > 0)
            {
                List<Provider_Product> providersProduct = _providerProductService.FindByProductId(obj.Id);

                providers.ForEach(element => {
                    var aux = providersProduct.Find(x => x.ProviderId == element.ProviderId);
                    if(aux == null)
                    {
                        Provider_Product providerProduct = new Provider_Product();
                        providerProduct.ProductId = product.Id;
                        providerProduct.ProviderId = element.ProviderId;

                        _providerProductService.Insert(providerProduct);
                    }
                });


                providersProduct.ForEach(element => {
                    var aux = providers.Find(x => x.ProviderId == element.ProviderId);
                    if (aux == null)
                    {
                        _providerProductService.Delete(element.Id);
                    }
                });
            }

            return product;
        }

        public Product Find(int value)
        {
            return base.Find(value);
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public override void Delete(int id)
        {
            base.Delete(id);
        }

        public List<Product> FindAllByUserId(int value)
        {
            return _productRepository.FindAllByUserId(value);
        }

    }
}
