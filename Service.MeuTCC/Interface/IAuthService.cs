﻿using MeuTCC.Domain.Authorization;

namespace MeuTCC.Service.Interface
{
    public interface IAuthService
    {
        public Token GetTokenAuth(string userEmail, string password, bool encripted = false);

        public Token GetTokenFromGoogleCredentials(string accessKey, GoogleCredentials credentials);

        public Token RefreshToken(string refreshToken);
    }
}
