﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Service.Interface
{
    public interface IUtilService
    {
        public bool ValidateCPF(string document);
    }
}
