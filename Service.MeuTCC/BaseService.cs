﻿using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using MeuTCC.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MeuTCC.Service
{
    public class BaseService<T> : IService<T> where T : BaseEntity
    {
        public BaseService()
        {
        }

        protected virtual BaseRepository<T> Repository { get; set; } = new BaseRepository<T>();
        protected virtual BaseRepository<Audit> AuditRepository { get; set; } = new BaseRepository<Audit>();

        public virtual T Insert(T obj)
        {
            Repository.Insert(obj);

            Log(obj, "Criado");

            return obj;
        }

        public virtual T Update(T obj)
        {
            Repository.Update(obj);

            Log(obj, "Editado");

            return obj;
        }

        public virtual void Delete(int id)
        {
            T obj = Repository.Find(id);
            Repository.Delete(id);

            Log(obj, "Removido");
        }

        public virtual T Find(int value)
        {
            return Repository.Find(value);
        }

        public virtual T Single(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return Repository.Single(Expressions);
        }

        public virtual IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return Repository.Select(Expressions);
        }

        public virtual PagedCollection<T> PagedSelect(List<Expression<Func<T, bool>>> Expression = null, int page = 1, int pageSize = 10)
        {
            return new PagedCollection<T>
            {
                Items = Repository.PagedSelect(Expression, null, page, pageSize),
                PaginationData = new PaginationData
                {
                    Count = Repository.Count(Expression),
                    PageSize = pageSize,
                    CurrentPage = page
                }
            };
        }

        #region Util
        protected void Log(T obj, string acao)
        {
            if (!(obj is Audit))
            {
                AuditRepository.Insert(new Audit
                {
                    ItemId = obj.Id,
                    Domain = obj.ObjectName,
                    Action = acao,
                    Json = obj.ToJson()
                });
            }
        }
        #endregion
    }
}
