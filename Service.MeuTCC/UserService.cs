﻿using MeuTCC.Data.Context;
using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Service
{
    public class UserService<T> : BaseService<T> where T : User
    {
        BaseService<Address> _addressService;
        AddressService<Address> _addressService2;
        UtilService _utilService;

        public UserService()
        {
            _addressService = new BaseService<Address>();
            _addressService2 = new AddressService<Address>();
            _utilService = new UtilService();
        }

        public User Insert(T obj)
        {
            dynamic _address = obj.Address;
            obj.Address = null;
            obj.Password = CryptographyService.Crypt(obj.Password);
            obj.Document = string.IsNullOrEmpty(obj.Document) ? string.Empty : StringUtilService.RemoveNonNumeric(obj.Document);

            User user = base.Insert(obj);

            if (_address != null)
            {
                _address.UserId = user.Id;
                Address address = _addressService.Insert(_address);
                user.Address = address;
            }

            return user;
        }

        public User Update(T obj)
        {
            dynamic _address = obj.Address;
            obj.Address = null;
            obj.Password = CryptographyService.Crypt(obj.Password);
            obj.Document = string.IsNullOrEmpty(obj.Document) ? string.Empty : StringUtilService.RemoveNonNumeric(obj.Document); 
            User user = base.Update(obj);

            if (_address != null)
            {
                _address.UserId = user.Id;
                Address address = _addressService.Update(_address);
                user.Address = address;
            }
            return user;
        }

        public User Find(int value)
        {
            User user = new User();
            user = base.Find(value);
            user.Address = _addressService2.FindAddressByUserId(user.Id);
            return user;
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public override void Delete(int id)
        {
            Address address = _addressService2.FindAddressByUserId(id);
            if(address != null)
            {
                _addressService.Delete(address.Id);
            }
            base.Delete(id);
        }
    }
}
