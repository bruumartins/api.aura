﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Service
{
    public static class StringUtilService
    {
        public static string RemoveNonNumeric(string text)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"[^0-9]");
            string ret = reg.Replace(text, string.Empty);
            return ret;
        }

        public static string FormatCPF(string CPF)
        {
            CPF = RemoveNonNumeric(CPF);
            return Convert.ToUInt64(CPF).ToString(@"000\.000\.000\-00");
        }
    }
}
