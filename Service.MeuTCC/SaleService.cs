﻿using MeuTCC.Data.Context;
using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Service
{
    public class SaleService<T> : BaseService<T> where T : Sale
    {
        BaseService<Sale_Product> _saleService;
        SaleRepository<Sale> _saleRepository;
        SaleProductService<Sale_Product> _saleProductService;
        public SaleService()
        {
            _saleService = new BaseService<Sale_Product>();
            _saleRepository = new SaleRepository<Sale>();
            _saleProductService = new SaleProductService<Sale_Product>();
        }

        public Sale Insert(T obj)
        {

            var products = obj.Products;
            obj.Products = null;

            Sale sale = base.Insert(obj);

            if(products != null && products.Count > 0) 
            {
                products.ForEach(element => {
                    Sale_Product saleProduct = new Sale_Product();
                    saleProduct.ProductId = element.ProductId;
                    saleProduct.SaleId = sale.Id;

                    _saleService.Insert(saleProduct);
                });
            }

            return sale;
        }

        public Sale Update(T obj)
        {
            var products = obj.Products;
            obj.Products = null;
            Sale sale = base.Update(obj);

            if (products != null && products.Count > 0)
            {
                List<Sale_Product> salesProduct = _saleProductService.FindBySaleId(obj.Id);

                products.ForEach(element => {
                    var aux = salesProduct.Find(x => x.ProductId == element.ProductId);
                    if (aux == null)
                    {
                        Sale_Product saleProduct = new Sale_Product();
                        saleProduct.ProductId = element.ProductId;
                        saleProduct.SaleId = sale.Id;

                        _saleService.Insert(saleProduct);
                    }
                });

                salesProduct.ForEach(element => {
                    var aux = products.Find(x => x.ProductId == element.ProductId);
                    if (aux == null)
                    {
                        _saleProductService.Delete(element.Id);
                    }
                });
            } else {
                List<Sale_Product> salesProduct = _saleProductService.FindBySaleId(obj.Id);
                salesProduct.ForEach(element => {
                    _saleProductService.Delete(element.Id);
                });
            }

            return sale;
        }

        public Sale Find(int value)
        {
            Sale sale = base.Find(value);
            return sale;
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public override void Delete(int id)
        {
            base.Delete(id);
        }

        public List<Sale> FindAllByUserId(int value)
        {
            return _saleRepository.FindAllByUserId(value);
        }

    }
}
