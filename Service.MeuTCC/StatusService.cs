﻿using MeuTCC.Data.Context;
using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Service
{
    public class StatusService<T> : BaseService<T> where T : Status
    {
        public StatusService()
        {
        }

        public override T Insert(T obj)
        {
            obj.Title = obj.Title.ToUpperInvariant();
            return base.Insert(obj);
        }

        public override T Update(T obj)
        {
            obj.Title = obj.Title.ToUpperInvariant();
            return base.Update(obj);
        }

        public override T Find(int value)
        {
            return base.Find(value);
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

    }
}
