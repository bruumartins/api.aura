﻿using MeuTCC.Data.Context;
using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Service
{
    public class ClientService<T> : BaseService<T> where T : Client
    {
        BaseService<Address> _addressService;
        AddressService<Address> _addressService2;
        ClientRepository<Client> _clientRepository;
        public ClientService()
        {
            _addressService = new BaseService<Address>();
            _addressService2 = new AddressService<Address>();
            _clientRepository = new ClientRepository<Client>();
        }

        public Client Insert(T obj)
        {
            dynamic _address = obj.Address;
            obj.Address = null;
            obj.Document = string.IsNullOrEmpty(obj.Document) ? string.Empty : StringUtilService.RemoveNonNumeric(obj.Document);

            Client client = base.Insert(obj);

            if (_address != null)
            {
                _address.ClientId = client.Id;
                Address address = _addressService.Insert(_address);
                client.Address = address;
            }

            return client;
        }

        public Client Update(T obj)
        {
            dynamic _address = obj.Address;
            obj.Address = null;
            obj.Document = string.IsNullOrEmpty(obj.Document) ? string.Empty : StringUtilService.RemoveNonNumeric(obj.Document);
            Client client = base.Update(obj);

            if (_address != null)
            {
                _address.ClientId = client.Id;
                var auxClient = _addressService2.FindAddressByClientId(client.Id);
                
                if (auxClient != null)
                {
                    _address.Id = auxClient.Id;
                    Address address = _addressService.Update(_address);
                    client.Address = address;
                }
                else
                {
                    Address address = _addressService.Insert(_address);
                    client.Address = address;
                }
                
            }
            return client;
        }

        public Client Find(int value)
        {
            Client client = new Client();
            client = base.Find(value);
            client.Address = _addressService2.FindAddressByClientId(client.Id);
            return client;
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public override void Delete(int id)
        {
            Address address = _addressService2.FindAddressByClientId(id);
            if(address != null)
            {
                _addressService.Delete(address.Id);
            }
            base.Delete(id);
        }
        public List<Client> FindAllByUserId(int value)
        {
            return _clientRepository.FindAllByUserId(value);
        }

    }
}
