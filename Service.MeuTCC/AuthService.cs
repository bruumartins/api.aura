﻿using MeuTCC.Domain.Authorization;
using MeuTCC.Domain.Entities;
using MeuTCC.Service.Interface;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;

namespace MeuTCC.Service
{
    public class AuthService : IAuthService
    {
        private readonly ILogger<AuthService> _logger;
        private readonly UserService<User> _userService;


        public AuthService(
            ILogger<AuthService> logger
        )
        {
            _logger = logger;
            _userService = new UserService<User>();
        }

        public Token GetTokenAuth(string userEmail, string password, bool encripted = false)
        {
            try
            {
                var expressions = new List<Expression<Func<User, bool>>>();
                expressions.Add(x => x.Email == userEmail);
                expressions.Add(x => x.Password == (encripted ? password : CryptographyService.Crypt(password)));

                var result = _userService.Select(expressions);

                if (result == null || result.Count == 0)
                {
                    return new Token { status = System.Net.HttpStatusCode.NotFound, message = "Usuário ou senha inválido." };
                }

                string token = TokenService.GenerateToken(result[0]);
                string refreshToken = WriteRefresh(result[0].Id);

                return new Token
                {
                    token = token,
                    userType = result[0].Type.ToString(),
                    refreshToken = refreshToken
                };
            }
            catch (System.Exception ex)
            {
                _logger.LogError("AuthService GetTokenAuth");
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                throw;
            }
        }

        public Token GetTokenFromGoogleCredentials(string accessKey, GoogleCredentials credentials)
        {
            try
            {
                Token token = new Token();

                using (var client = new HttpClient(new HttpClientHandler() { AllowAutoRedirect = true, AutomaticDecompression = System.Net.DecompressionMethods.Deflate | System.Net.DecompressionMethods.GZip }))
                {
                    client.Timeout = TimeSpan.FromMinutes(5);
                    StringBuilder sb = new StringBuilder();
                    sb.AppendFormat(credentials.ValidationUrl, accessKey);

                    HttpResponseMessage response = client.GetAsync(sb.ToString()).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var auth = JsonConvert.DeserializeObject<GoogleAuth>(response.Content.ReadAsStringAsync().Result);
                        if (credentials.ClientId.Contains(auth.aud))
                        {
                            User user;
                            var filterGoogleId = new List<Expression<Func<User, bool>>>();
                            filterGoogleId.Add(x => x.GoogleGuid == auth.sub);
                            user = _userService.Select(filterGoogleId).FirstOrDefault();
                            if (user == null)
                            {
                                //Caso não encontre o usuário pelo GoogleID verifica se tem algum usuário com o mesmo e-mail e associo a conta do Google

                                var filterEmail = new List<Expression<Func<User, bool>>>();
                                filterEmail.Add(x => x.Email == auth.email);

                                user = _userService.Select(filterEmail).FirstOrDefault();
                                if (user != null)
                                {
                                    user.GoogleGuid = auth.sub;
                                    user = _userService.Update(user);

                                    var authToken = TokenService.GenerateToken(user);
                                    token.token = authToken;
                                    token.userType = user.Type.ToString();
                                }
                                else
                                {
                                    token.status = System.Net.HttpStatusCode.NotFound;
                                    token.message = "Usuário não localizado.";
                                }
                            }
                            else
                            {
                                var authToken = TokenService.GenerateToken(user);
                                token.token = authToken;
                                token.userType = user.Type.ToString();
                            }
                        }
                        else
                        {
                            token.status = System.Net.HttpStatusCode.Unauthorized;
                            token.message = "Token inválido.";
                        }
                    }
                    else
                    {
                        token.status = System.Net.HttpStatusCode.Unauthorized;
                        token.message = "Token inválido.";
                    }
                }

                return token;
            }
            catch (Exception ex)
            {
                _logger.LogError("AuthService GetTokenFromGoogleCredentials");
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                throw;
            }
        }

        private string WriteRefresh(int user_id)
        {
            return CryptographyService.Crypt($"{user_id}#{DateTime.UtcNow.AddDays(40).Ticks}");
        }

        public Token RefreshToken(string refreshToken)
        {
            Token token = new Token();

            if (!string.IsNullOrWhiteSpace(refreshToken))
            {
                refreshToken = CryptographyService.Decrypt(refreshToken);

                string[] parts = refreshToken.Split('#');
                if (parts.Length == 2)
                {
                    DateTime end = new DateTime(Convert.ToInt64(parts[1]));
                    if (DateTime.UtcNow <= end)
                    {
                        var user = _userService.Find(int.Parse(parts[0]));
                        if (user != null)
                        {
                            token.userType = user.Type.ToString();
                            token.refreshToken = WriteRefresh(user.Id);
                            token.token = TokenService.GenerateToken(user);
                        }
                    }
                }
            }

            return token;
        }
    }
}
