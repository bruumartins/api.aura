﻿using MeuTCC.Data.Context;
using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Service
{
    public class AddressService<T> : BaseService<T> where T : Address
    {

        AddressRepository<Address> addressRepository;
        public AddressService()
        {
            addressRepository = new AddressRepository<Address>();
        }

        public override T Insert(T obj)
        {
            obj.CEP = string.IsNullOrEmpty(obj.CEP) ? string.Empty : StringUtilService.RemoveNonNumeric(obj.CEP);
            return base.Insert(obj);
        }

        public override T Update(T obj)
        {
            obj.CEP = string.IsNullOrEmpty(obj.CEP) ? string.Empty : StringUtilService.RemoveNonNumeric(obj.CEP);
            return base.Update(obj);
        }

        public override T Find(int value)
        {
            return base.Find(value);
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public Address FindAddressByUserId(int value)
        {
            return addressRepository.FindAddressByUserId(value);
        }

        public Address FindAddressByClientId(int value)
        {
            return addressRepository.FindAddressByClientId(value);
        }

        public Address FindAddressByProviderId(int value)
        {
            return addressRepository.FindAddressByProviderId(value);
        }
    }
}
