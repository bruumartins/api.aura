﻿using MeuTCC.Data.Context;
using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Service
{
    public class ProviderService<T> : BaseService<T> where T : Provider
    {
        BaseService<Address> _addressService;
        AddressService<Address> _addressService2;
        BaseService<Provider_Product> _productsService;
        ProviderRepository<Provider> _providerRepository;
        public ProviderService()
        {
            _addressService = new BaseService<Address>();
            _addressService2 = new AddressService<Address>();
            _productsService = new BaseService<Provider_Product>();
            _providerRepository = new ProviderRepository<Provider>();
        }

        public Provider Insert(T obj)
        {

            dynamic _address = obj.Address;
            obj.Address = null;
            obj.Document = string.IsNullOrEmpty(obj.Document) ? string.Empty : StringUtilService.RemoveNonNumeric(obj.Document);

            var products = obj.Products;
            obj.Products = null;

            Provider provider = base.Insert(obj);

            if(products != null && products.Count > 0) 
            {
                products.ForEach(element => {
                    Provider_Product providerProduct = new Provider_Product();
                    providerProduct.ProductId = element.ProductId;
                    providerProduct.ProviderId = provider.Id;

                    _productsService.Insert(providerProduct);
                });
            }

            if (_address != null)
            {
                _address.ProviderId = provider.Id;
                Address address = _addressService.Insert(_address);
                provider.Address = address;
            }

            return provider;
        }

        public Provider Update(T obj)
        {
            dynamic _address = obj.Address;
            obj.Address = null;
            obj.Document = string.IsNullOrEmpty(obj.Document) ? string.Empty : StringUtilService.RemoveNonNumeric(obj.Document);
            Provider provider = base.Update(obj);

            if (_address != null)
            {
                _address.ProviderId = provider.Id;
                var auxProvider = _addressService2.FindAddressByProviderId(provider.Id);

                if (auxProvider != null)
                {
                    _address.Id = auxProvider.Id;
                    Address address = _addressService.Update(_address);
                    provider.Address = address;
                }
                else
                {
                    Address address = _addressService.Insert(_address);
                    provider.Address = address;
                }
            }
            return provider;
        }

        public Provider Find(int value)
        {
            Provider provider = new Provider();
            provider = base.Find(value);
            provider.Address = _addressService2.FindAddressByProviderId(provider.Id);
            return provider;
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public override void Delete(int id)
        {
            Address address = _addressService2.FindAddressByProviderId(id);
            if(address != null)
            {
                _addressService.Delete(address.Id);
            }
            base.Delete(id);
        }

        public List<Provider> FindAllByUserId(int value)
        {
            return _providerRepository.FindAllByUserId(value);
        }

    }
}
