﻿using MeuTCC.Data.Context;
using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace MeuTCC.Service
{
    public class ExpenseService<T> : BaseService<T> where T : Expense
    {
        BaseService<Expense_Product> _expenseService;
        ExpenseRepository<Expense> _expenseRepository;
        ExpenseProductService<Expense_Product> _expenseProductService;
        public ExpenseService()
        {
            _expenseService = new BaseService<Expense_Product>();
            _expenseRepository = new ExpenseRepository<Expense>();
            _expenseProductService = new ExpenseProductService<Expense_Product>();
        }

        public Expense Insert(T obj)
        {

            var products = obj.Products;
            obj.Products = null;

            Expense expense = base.Insert(obj);

            if(products != null && products.Count > 0) 
            {
                products.ForEach(element => {
                    Expense_Product expenseProduct = new Expense_Product();
                    expenseProduct.ProductId = element.ProductId;
                    expenseProduct.ExpenseId = expense.Id;

                    _expenseService.Insert(expenseProduct);
                });
            }

            return expense;
        }

        public Expense Update(T obj)
        {
            var products = obj.Products;
            obj.Products = null;
            Expense expense = base.Update(obj);

            if (products != null && products.Count > 0)
            {
                List<Expense_Product> expensesProduct = _expenseProductService.FindByExpenseId(obj.Id);

                products.ForEach(element => {
                    var aux = expensesProduct.Find(x => x.ProductId == element.ProductId);
                    if (aux == null)
                    {
                        Expense_Product expenseProduct = new Expense_Product();
                        expenseProduct.ProductId = element.ProductId;
                        expenseProduct.ExpenseId = expense.Id;

                        _expenseService.Insert(expenseProduct);
                    }
                });


                expensesProduct.ForEach(element => {
                    var aux = products.Find(x => x.ProductId == element.ProductId);
                    if (aux == null)
                    {
                        _expenseProductService.Delete(element.Id);
                    }
                });
            } else {
                List<Expense_Product> expensesProduct = _expenseProductService.FindByExpenseId(obj.Id);
                expensesProduct.ForEach(element => {
                    _expenseProductService.Delete(element.Id);
                });
            }

            return expense;
        }

        public Expense Find(int value)
        {
            Expense expense = base.Find(value);
            return expense;
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public override void Delete(int id)
        {
            base.Delete(id);
        }

        public List<Expense> FindAllByUserId(int value)
        {
            return _expenseRepository.FindAllByUserId(value);
        }
    }
}
