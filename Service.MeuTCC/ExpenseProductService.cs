﻿using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MeuTCC.Service
{
    public class ExpenseProductService<T> : BaseService<T> where T : Expense_Product
    {
        ExpenseProductRepository<Expense_Product> _expenseProductRepository;
        public ExpenseProductService()
        {
            _expenseProductRepository = new ExpenseProductRepository<Expense_Product>();
        }
        public override T Insert(T obj)
        {
            return base.Insert(obj);
        }

        public override T Update(T obj)
        {
            return base.Update(obj);
        }

        public override T Find(int value)
        {
            return base.Find(value);
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public List<Expense_Product> FindByExpenseId(int value)
        {
            return _expenseProductRepository.FindByExpenseId(value);
        }

        public List<Expense_Product> FindByProductId(int value)
        {
            return _expenseProductRepository.FindByProductId(value);
        }

    }
}
