﻿using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MeuTCC.Service
{
    public class SaleProductService<T> : BaseService<T> where T : Sale_Product
    {
        SaleProductRepository<Sale_Product> _saleProductRepository;
        public SaleProductService()
        {
            _saleProductRepository = new SaleProductRepository<Sale_Product>();
        }
        public override T Insert(T obj)
        {
            return base.Insert(obj);
        }

        public override T Update(T obj)
        {
            return base.Update(obj);
        }

        public override T Find(int value)
        {
            return base.Find(value);
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public List<Sale_Product> FindBySaleId(int value)
        {
            return _saleProductRepository.FindBySaleId(value);
        }

        public List<Sale_Product> FindByProductId(int value)
        {
            return _saleProductRepository.FindByProductId(value);
        }

    }
}
