﻿using MeuTCC.Data.Context;

namespace MeuTCC.Service
{
    public static class InitializeService
    {
        public static void Initialize()
        {
            InitializeDB.Initialize();
        }
    }
}
