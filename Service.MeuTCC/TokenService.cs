﻿using MeuTCC.Domain.Config;
using MeuTCC.Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace MeuTCC.Service
{
    public static class TokenService
    {
        public static string GenerateToken(User user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Settings.Secret);
            var expired = DateTime.UtcNow.AddHours(8);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(
                    new Claim[] {
                        new Claim("Id", user.Id.ToString()),
                        new Claim(ClaimTypes.Name, user.Name),
                        new Claim("Email", user.Email),
                        new Claim("ObjectName", user.ObjectName),
                        new Claim(ClaimTypes.Role, user.Type.ToString().ToLower()),
                        new Claim("Document", user.Document.ToString()),
                        new Claim(ClaimTypes.Expired, expired.ToString()),
                    }
                ),
                Expires = expired,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
