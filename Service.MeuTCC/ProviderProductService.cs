﻿using MeuTCC.Data.Repository;
using MeuTCC.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MeuTCC.Service
{
    public class ProviderProductService<T> : BaseService<T> where T : Provider_Product
    {
        ProviderProductRepository<Provider_Product> providerProductRepository;
        public ProviderProductService()
        {
            providerProductRepository = new ProviderProductRepository<Provider_Product>();
        }
        public override T Insert(T obj)
        {
            return base.Insert(obj);
        }

        public override T Update(T obj)
        {
            return base.Update(obj);
        }

        public override T Find(int value)
        {
            return base.Find(value);
        }

        public override IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null)
        {
            return base.Select(Expressions);
        }

        public List<Provider_Product> FindByProviderId(int value)
        {
            return providerProductRepository.FindByProviderId(value);
        }

        public List<Provider_Product> FindByProductId(int value)
        {
            return providerProductRepository.FindByProductId(value);
        }

    }
}
