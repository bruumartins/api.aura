﻿namespace MeuTCC.Data.Context
{
    public static class InitializeDB
    {
        public static void Initialize()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            //context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }
    }
}
