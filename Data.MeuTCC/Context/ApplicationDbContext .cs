﻿using MeuTCC.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;

namespace MeuTCC.Data.Context
{
    public class ApplicationDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                IConfigurationRoot Configuration = new ConfigurationBuilder()
                    .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
                    .AddJsonFile("appsettings.json")
                    .Build();

                optionsBuilder.UseMySql(Configuration.GetConnectionString("DefaultConnection"));
            }
        }

        public DbSet<Audit> Audit { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Client> Client { get; set; }
        public DbSet<Expense> Expense { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<Provider> Provider { get; set; }
        public DbSet<Reminder> Reminder { get; set; }
        public DbSet<Sale> Sale { get; set; }
        public DbSet<Status> Status { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Sale_Product> Sale_Product { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Audit>().HasKey(cc => cc.Id);

            modelBuilder.Entity<Address>().HasKey(cc => cc.Id);
            modelBuilder.Entity<Address>().Property(cc => cc.Created)
                .ValueGeneratedOnAdd()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Address>().Property(cc => cc.Modified)
                .ValueGeneratedOnAddOrUpdate()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");

            modelBuilder.Entity<Client>().HasKey(cc => cc.Id);
            modelBuilder.Entity<Client>().Property(cc => cc.Created)
                .ValueGeneratedOnAdd()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Client>().Property(cc => cc.Modified)
                .ValueGeneratedOnAddOrUpdate()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Client>().HasOne(bc => bc.Address)
                .WithOne(b => b.Client).HasForeignKey<Address>(c => c.ClientId);

            modelBuilder.Entity<Expense>().HasKey(cc => cc.Id);
            modelBuilder.Entity<Expense>().Property(cc => cc.Created)
                .ValueGeneratedOnAdd()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Expense>().Property(cc => cc.Modified)
                .ValueGeneratedOnAddOrUpdate()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Expense>().HasOne(bc => bc.Status);

            modelBuilder.Entity<Product>().HasKey(cc => cc.Id);
            modelBuilder.Entity<Product>().Property(cc => cc.Created)
                .ValueGeneratedOnAdd()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Product>().Property(cc => cc.Modified)
                .ValueGeneratedOnAddOrUpdate()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");

            modelBuilder.Entity<Provider>().HasKey(cc => cc.Id);
            modelBuilder.Entity<Provider>().Property(cc => cc.Created)
                .ValueGeneratedOnAdd()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Provider>().Property(cc => cc.Modified)
                .ValueGeneratedOnAddOrUpdate()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Provider>().HasOne(bc => bc.Address)
                .WithOne(b => b.Provider).HasForeignKey<Address>(c => c.ProviderId);

            modelBuilder.Entity<Reminder>().HasKey(cc => cc.Id);
            modelBuilder.Entity<Reminder>().Property(cc => cc.Created)
                .ValueGeneratedOnAdd()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Reminder>().Property(cc => cc.Modified)
                .ValueGeneratedOnAddOrUpdate()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");

            modelBuilder.Entity<Sale>().HasKey(cc => cc.Id);
            modelBuilder.Entity<Sale>().Property(cc => cc.Created)
                .ValueGeneratedOnAdd()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Sale>().Property(cc => cc.Modified)
                .ValueGeneratedOnAddOrUpdate()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Sale>().HasOne(bc => bc.Client);
            modelBuilder.Entity<Sale>().HasOne(bc => bc.Status);

            modelBuilder.Entity<Status>().HasKey(cc => cc.Id);
            modelBuilder.Entity<Status>().Property(cc => cc.Created)
                .ValueGeneratedOnAdd()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Status>().Property(cc => cc.Modified)
                .ValueGeneratedOnAddOrUpdate()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");

            modelBuilder.Entity<User>().HasKey(cc => cc.Id);
            modelBuilder.Entity<User>().Property(cc => cc.Created)
                .ValueGeneratedOnAdd()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<User>().Property(cc => cc.Modified)
                .ValueGeneratedOnAddOrUpdate()
                .HasColumnType("datetime")
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<User>().HasOne(bc => bc.Address)
                .WithOne(b => b.User).HasForeignKey<Address>(c => c.UserId);

            modelBuilder.Entity<Sale_Product>()
                .HasKey(cc => new { cc.SaleId, cc.ProductId });
            modelBuilder.Entity<Sale_Product>()
                .HasOne(bc => bc.Sale)
                .WithMany(b => b.Products)
                .HasForeignKey(bc => bc.SaleId);
            modelBuilder.Entity<Sale_Product>()
                .HasOne(bc => bc.Product)
                .WithMany(b => b.Sales)
                .HasForeignKey(bc => bc.ProductId);

            modelBuilder.Entity<Expense_Product>()
                .HasKey(cc => new { cc.ExpenseId, cc.ProductId });
            modelBuilder.Entity<Expense_Product>()
                .HasOne(bc => bc.Expense)
                .WithMany(b => b.Products)
                .HasForeignKey(bc => bc.ExpenseId);
            modelBuilder.Entity<Expense_Product>()
                .HasOne(bc => bc.Product)
                .WithMany(b => b.Expenses)
                .HasForeignKey(bc => bc.ProductId);

            modelBuilder.Entity<Provider_Product>()
                .HasKey(cc => new { cc.ProviderId, cc.ProductId });
            modelBuilder.Entity<Provider_Product>()
                .HasOne(bc => bc.Provider)
                .WithMany(b => b.Products)
                .HasForeignKey(bc => bc.ProviderId);
            modelBuilder.Entity<Provider_Product>()
                .HasOne(bc => bc.Product)
                .WithMany(b => b.Providers)
                .HasForeignKey(bc => bc.ProductId);

            base.OnModelCreating(modelBuilder);
        }
    }
}
