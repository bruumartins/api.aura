﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class ExpenseProductRepository<T> : Expense_Product
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual List<Expense_Product> FindByExpenseId(int value, IQueryable<Expense_Product> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Expense_Product>();
            }

            return queryable.Where(x => x.ExpenseId == value).ToList();
        }

        public virtual List<Expense_Product> FindByProductId(int value, IQueryable<Expense_Product> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Expense_Product>();
            }

            return queryable.Where(x => x.ProductId == value).ToList();
        }

    }
}
