﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class ExpenseRepository<T> : Expense
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual List<Expense> FindAllByUserId(int value, IQueryable<Expense> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Expense>();
            }

            return queryable.Where(x => x.UserId == value).ToList();
        }

    }
}
