﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class ProviderRepository<T> : Provider
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual List<Provider> FindAllByUserId(int value, IQueryable<Provider> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Provider>();
            }

            return queryable.Where(x => x.UserId == value).ToList();
        }

    }
}
