﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class AddressRepository<T> : Address
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual Address FindAddressByUserId(int value, IQueryable<Address> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Address>();
            }

            return queryable.FirstOrDefault(x => x.UserId == value);
        }

        public virtual Address FindAddressByClientId(int value, IQueryable<Address> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Address>();
            }

            return queryable.FirstOrDefault(x => x.ClientId == value);
        }

        public virtual Address FindAddressByProviderId(int value, IQueryable<Address> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Address>();
            }

            return queryable.FirstOrDefault(x => x.ProviderId == value);
        }

    }
}
