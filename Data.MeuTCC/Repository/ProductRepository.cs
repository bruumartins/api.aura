﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class ProductRepository<T> : Product
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual List<Product> FindAllByUserId(int value, IQueryable<Product> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Product>();
            }

            return queryable.Where(x => x.UserId == value).ToList();
        }

    }
}
