﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class SaleRepository<T> : Sale
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual List<Sale> FindAllByUserId(int value, IQueryable<Sale> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Sale>();
            }

            return queryable.Where(x => x.UserId == value).ToList();
        }

    }
}
