﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class SaleProductRepository<T> : Sale_Product
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual List<Sale_Product> FindBySaleId(int value, IQueryable<Sale_Product> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Sale_Product>();
            }

            return queryable.Where(x => x.SaleId == value).ToList();
        }

        public virtual List<Sale_Product> FindByProductId(int value, IQueryable<Sale_Product> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Sale_Product>();
            }

            return queryable.Where(x => x.ProductId == value).ToList();
        }

    }
}
