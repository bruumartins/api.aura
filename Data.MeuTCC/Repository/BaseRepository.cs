﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public void Insert(T obj)
        {
            obj.Created = DateTime.Now;
            obj.Modified = DateTime.Now;

            context.Set<T>().Add(obj);
            context.SaveChanges();
        }

        public void Update(T obj)
        {
            obj.Modified = DateTime.Now;

            context.Entry(obj).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void Delete(int id)
        {
            context.Set<T>().Remove(Find(id));
            context.SaveChanges();
        }

        public virtual T Find(int value, IQueryable<T> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<T>();
            }

            return queryable.FirstOrDefault(x => x.Id == value);
        }

        public virtual T Single(List<Expression<Func<T, bool>>> Expressions = null, IQueryable<T> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<T>();
            }

            if (Expressions != null)
            {
                foreach (var expression in Expressions)
                {
                    queryable = queryable.Where(expression);
                }
            }

            return queryable.OrderByDescending(o => o.Created).FirstOrDefault();
        }

        public virtual IList<T> Select(List<Expression<Func<T, bool>>> Expressions = null, IQueryable<T> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<T>();
            }

            if (Expressions != null)
            {
                foreach (var expression in Expressions)
                {
                    queryable = queryable.Where(expression);
                }
            }

            return queryable.OrderByDescending(o => o.Created).ToList();
        }

        public virtual IList<T> PagedSelect(List<Expression<Func<T, bool>>> Expressions = null, IQueryable<T> queryable = null, int page = 1, int pageSize = 10)
        {
            if (queryable == null)
            {
                queryable = context.Set<T>();
            }

            if (Expressions != null)
            {
                foreach (var expression in Expressions)
                {
                    queryable = queryable.Where(expression);
                }
            }

            return queryable.OrderByDescending(x => x.Created).Skip((page - 1) * pageSize).Take(pageSize).ToList();
        }

        public virtual int Count(List<Expression<Func<T, bool>>> Expressions = null)
        {
            IQueryable<T> queryable = context.Set<T>();

            if (Expressions != null)
            {
                foreach (var expression in Expressions)
                {
                    queryable = queryable.Where(expression);
                }
            }

            return queryable.Count();
        }
    }
}
