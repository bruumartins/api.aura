﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class ProviderProductRepository<T> : Provider_Product
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual List<Provider_Product> FindByProviderId(int value, IQueryable<Provider_Product> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Provider_Product>();
            }

            return queryable.Where(x => x.ProviderId == value).ToList();
        }

        public virtual List<Provider_Product> FindByProductId(int value, IQueryable<Provider_Product> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Provider_Product>();
            }

            return queryable.Where(x => x.ProductId == value).ToList();
        }

    }
}
