﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class ClientRepository<T> : Client
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual List<Client> FindAllByUserId(int value, IQueryable<Client> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Client>();
            }

            return queryable.Where(x => x.UserId == value).ToList();
        }

    }
}
