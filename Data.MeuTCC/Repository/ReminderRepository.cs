﻿using MeuTCC.Data.Context;
using MeuTCC.Domain.Entities;
using MeuTCC.Domain.Intarfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;


namespace MeuTCC.Data.Repository
{
    public class ReminderRepository<T> : Reminder
    {
        protected ApplicationDbContext context = new ApplicationDbContext();

        public virtual List<Reminder> FindAllByUserId(int value, IQueryable<Reminder> queryable = null)
        {
            if (queryable == null)
            {
                queryable = context.Set<Reminder>();
            }

            return queryable.Where(x => x.UserId == value).ToList();
        }

    }
}
