﻿using MeuTCC.Domain.Entities;
using MeuTCC.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;

namespace MeuTCC.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ExpenseController : ControllerBase
    {
        private readonly ILogger<ExpenseController> _logger;
        private readonly ExpenseService<Expense> _expenseService;
        private readonly ExpenseProductService<Expense_Product> _expenseProductService;
        public ExpenseController(ILogger<ExpenseController> logger)
        {
            _logger = logger;
            _expenseService = new ExpenseService<Expense>();
            _expenseProductService = new ExpenseProductService<Expense_Product>();
        }


        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult GetAll()
        {
            try
            {
                List<Expression<Func<Expense, bool>>> expressionList = new List<Expression<Func<Expense, bool>>>();
                var result = _expenseService.Select(expressionList);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ExpenseController - Get all error!");
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetAllByUserId")]
        [Authorize(Roles = "admin,user")]
        public IActionResult GetAllByUserId([FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var id = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                var result = _expenseService.FindAllByUserId(Int32.Parse(id));
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ExpenseController - GetAllByUserId error!");
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Get(int id)
        {
            try
            {
                Expense result = _expenseService.Find(id);
                result.Products = _expenseProductService.FindByExpenseId(id);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ExpenseController - Get by id error!");
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin,user")]
        public IActionResult Post(Expense expense, [FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var idUser = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                expense.UserId = Int32.Parse(idUser);
                Expense result = _expenseService.Insert(expense);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ExpenseController - Post error!");
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Put(Expense expense, [FromRoute] int id, [FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var idUser = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                expense.UserId = Int32.Parse(idUser);
                expense.Id = id;
                Expense result = _expenseService.Update(expense);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ExpenseController - Put error!");
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Delete(int id)
        {
            try
            {
                _expenseService.Delete(id);
                return new ObjectResult($"Item {id} deletado com sucesso.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ExpenseController - Delete error!");
                return BadRequest();
            }
        }
    }
}
