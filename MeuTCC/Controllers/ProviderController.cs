﻿using MeuTCC.Domain.Entities;
using MeuTCC.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;

namespace MeuTCC.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProviderController : ControllerBase
    {
        private readonly ILogger<ProviderController> _logger;
        private readonly ProviderService<Provider> _providerService; 
        public ProviderController(ILogger<ProviderController> logger)
        {
            _logger = logger;
            _providerService = new ProviderService<Provider>();
        }


        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult GetAll()
        {
            try
            {
                List<Expression<Func<Provider, bool>>> expressionList = new List<Expression<Func<Provider, bool>>>();
                var result = _providerService.Select(expressionList);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ProviderController - Get all error!");
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetAllByUserId")]
        [Authorize(Roles = "admin,user")]
        public IActionResult GetAllByUserId([FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var id = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                var result = _providerService.FindAllByUserId(Int32.Parse(id));
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ProviderController - GetAllByUserId error!");
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Get(int id)
        {
            try
            {
                Provider result = _providerService.Find(id);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ProviderController - Get by id error!");
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin,user")]
        public IActionResult Post(Provider provider, [FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var id = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                provider.UserId = Int32.Parse(id);
                Provider result = _providerService.Insert(provider);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ProviderController - Post error!");
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Put(Provider provider, [FromRoute] int id, [FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var idUser = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                provider.UserId = Int32.Parse(idUser);
                provider.Id = id;
                Provider result = _providerService.Update(provider);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ProviderController - Put error!");
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Delete(int id)
        {
            try
            {
                _providerService.Delete(id);
                return new ObjectResult($"Item {id} deletado com sucesso.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ProviderController - Delete error!");
                return BadRequest();
            }
        }
    }
}
