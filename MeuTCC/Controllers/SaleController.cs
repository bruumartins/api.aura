﻿using MeuTCC.Domain.Entities;
using MeuTCC.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;

namespace MeuTCC.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly ILogger<SaleController> _logger;
        private readonly SaleService<Sale> _saleService; 
        private readonly SaleProductService<Sale_Product> _saleProductService;
        public SaleController(ILogger<SaleController> logger)
        {
            _logger = logger;
            _saleService = new SaleService<Sale>();
            _saleProductService = new SaleProductService<Sale_Product>();
        }


        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult GetAll()
        {
            try
            {
                List<Expression<Func<Sale, bool>>> expressionList = new List<Expression<Func<Sale, bool>>>();
                var result = _saleService.Select(expressionList);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("SaleController - Get all error!");
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetAllByUserId")]
        [Authorize(Roles = "admin,user")]
        public IActionResult GetAllByUserId([FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var id = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                var result = _saleService.FindAllByUserId(Int32.Parse(id));
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("SaleController - GetAllByUserId error!");
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Get(int id)
        {
            try
            {
                Sale result = _saleService.Find(id);
                result.Products = _saleProductService.FindBySaleId(id);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("SaleController - Get by id error!");
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin,user")]
        public IActionResult Post(Sale sale, [FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var idUser = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                sale.UserId = Int32.Parse(idUser);
                Sale result = _saleService.Insert(sale);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("SaleController - Post error!");
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Put(Sale sale, [FromRoute] int id, [FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var idUser = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                sale.UserId = Int32.Parse(idUser);
                sale.Id = id;
                Sale result = _saleService.Update(sale);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("SaleController - Put error!");
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Delete(int id)
        {
            try
            {
                _saleService.Delete(id);
                return new ObjectResult($"Item {id} deletado com sucesso.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("SaleController - Delete error!");
                return BadRequest();
            }
        }
    }
}
