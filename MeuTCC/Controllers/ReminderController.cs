﻿using MeuTCC.Domain.Entities;
using MeuTCC.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;

namespace MeuTCC.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReminderController : ControllerBase
    {
        private readonly ILogger<ReminderController> _logger;
        private readonly ReminderService<Reminder> _reminderService;
        public ReminderController(ILogger<ReminderController> logger)
        {
            _logger = logger;
            _reminderService = new ReminderService<Reminder>();
        }

        [HttpGet]
        [Authorize(Roles = "admin")]
        public IActionResult GetAll()
        {
            try
            {
                List<Expression<Func<Reminder, bool>>> expressionList = new List<Expression<Func<Reminder, bool>>>();
                var result = _reminderService.Select(expressionList);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ReminderController - Get all error!");
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetAllByUserId")]
        [Authorize(Roles = "admin,user")]
        public IActionResult GetAllByUserId([FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var id = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                var result = _reminderService.FindAllByUserId(Int32.Parse(id));
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ReminderController - GetAllByUserId error!");
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Get(int id)
        {
            try
            {
                Reminder result = _reminderService.Find(id);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ReminderController - Get by id error!");
                return BadRequest();
            }
        }

        [HttpPost]
        [Authorize(Roles = "admin,user")]
        public IActionResult Post(Reminder reminder, [FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var id = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                reminder.UserId = Int32.Parse(id);
                Reminder result = _reminderService.Insert(reminder);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ReminderController - Post error!");
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Put(Reminder reminder, [FromRoute] int id, [FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;
                var idUser = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;

                reminder.UserId = Int32.Parse(idUser);
                reminder.Id = id;
                Reminder result = _reminderService.Update(reminder);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ReminderController - Put error!");
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Delete(int id)
        {
            try
            {
                _reminderService.Delete(id);
                return new ObjectResult($"Item {id} deletado com sucesso.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("ReminderController - Delete error!");
                return BadRequest();
            }
        }
    }
}
