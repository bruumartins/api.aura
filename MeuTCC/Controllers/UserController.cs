﻿using MeuTCC.Domain.Entities;
using MeuTCC.Service;
using MeuTCC.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Expressions;

namespace MeuTCC.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly UserService<User> _userService;
        private readonly IAuthService _authService;

        public UserController(
            ILogger<UserController> logger,
            IAuthService authService
        )
        {
            _authService = authService;
            _logger = logger;
            _userService = new UserService<User>();
        }


        [HttpGet]
        [Authorize(Roles = "admin,user")]
        public IActionResult GetAll()
        {
            try
            {
                List<Expression<Func<User, bool>>> expressionList = new List<Expression<Func<User, bool>>>();
                var result = _userService.Select(expressionList);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("UserController - Get all error!");
                return BadRequest();
            }
        }

        [HttpGet("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Get(int id)
        {
            try
            {
                User result = _userService.Find(id);
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("UserController - Get by id error!");
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetMyUser")]
        [Authorize(Roles = "admin,user")]
        public IActionResult GetMyUser([FromHeader] string authorization)
        {
            try
            {
                var token = authorization.Replace("Bearer ", "");
                var handler = new JwtSecurityTokenHandler();
                var jsonToken = handler.ReadToken(token);
                var tokenS = handler.ReadToken(token) as JwtSecurityToken;

                var id = tokenS.Claims.First(claim => claim.Type.Contains("Id")).Value;
                User result = _userService.Find(Int32.Parse(id));
                return new ObjectResult(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("UserController - Get my user error!");
                return BadRequest();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult Post([FromBody] User user)
        {
            try
            {
                User result = _userService.Insert(user);
                var token = _authService.GetTokenAuth(user.Email, user.Password, true);
                return new ObjectResult(token);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("UserController - Post error!");
                return BadRequest();
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Put([FromBody] User user, [FromRoute] int id)
        {
            try
            {
                user.Id = id;
                User result = _userService.Update(user);
                var token = _authService.GetTokenAuth(user.Email, user.Password, true);
                return new ObjectResult(token);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("UserController - Put error!");
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "admin,user")]
        public IActionResult Delete(int id)
        {
            try
            {
                _userService.Delete(id);
                return new ObjectResult($"Item {id} deletado com sucesso.");
            }
            catch (Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("UserController - Delete error!");
                return BadRequest();
            }
        }
    }
}
