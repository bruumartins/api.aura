﻿using MeuTCC.Domain.Authorization;
using MeuTCC.Service.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;

namespace MeuTCC.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<AuthController> _logger;
        private readonly IAuthService _authService;
        private readonly string[] grandTypes = { "google", "password", "refresh_token" };

        public AuthController(
            ILogger<AuthController> logger,
            IAuthService authService
        )
        {
            _logger = logger;
            _authService = authService;
        }


        [HttpPost]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] Login login, [FromServices] GoogleCredentials credentials)
        {
            try
            {
                Token token = null;

                string message = ValidateLogin(login);

                if (!string.IsNullOrEmpty(message))
                {
                    return BadRequest(new { message = message });
                }

                if (login.grantType == "password")
                {
                    token = _authService.GetTokenAuth(login.userEmail, login.accessKey);
                }
                else if (login.grantType == "google")
                {
                    token = _authService.GetTokenFromGoogleCredentials(login.accessKey, credentials);
                }

                if (token.status != 0)
                {
                    return ReturnError(token.status, token.message);
                }

                return new ObjectResult(token);
            }
            catch (System.Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("AuthController - Authenticate!");
                return BadRequest();
            }
        }


        [HttpPost]
        [Route("refrehToken")]
        [AllowAnonymous]
        public IActionResult RefreshToken([FromBody] Token token, [FromServices] GoogleCredentials credentials)
        {
            try
            {
                var newToken = _authService.RefreshToken(token.token);

                if (token.status != 0)
                {
                    return ReturnError(token.status, token.message);
                }

                return new ObjectResult(newToken);
            }
            catch (System.Exception ex)
            {
                _logger.LogError($"Message: {ex.Message}. Inner: {ex.InnerException?.Message}. Stack: {ex.StackTrace}");
                _logger.LogError("AuthController - Authenticate!");
                return BadRequest();
            }
        }

        private IActionResult ReturnError(System.Net.HttpStatusCode httpStatusCode, string message)
        {

            if (httpStatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                return Unauthorized(new { message = message });
            }
            else if (httpStatusCode == System.Net.HttpStatusCode.NotFound)
            {
                return NotFound(new { message = message });
            }

            return BadRequest(new { message = message });
        }


        private string ValidateLogin(Login login)
        {
            string message = string.Empty;
            login.grantType = login.grantType.ToLowerInvariant();

            if (login == null)
            {
                message = "Usuário não informado";
            }
            else if (string.IsNullOrEmpty(login.grantType))
            {
                message = "Grandtype não informado";
            }
            else if (!grandTypes.Contains(login.grantType))
            {
                message = "GradType inválido.";
            }
            else if (
                (string.IsNullOrEmpty(login.userEmail) || string.IsNullOrEmpty(login.accessKey))
                && string.IsNullOrEmpty(login.grantType))
            {
                message = "Usuário não informado";
            }

            return message;
        }
    }
}
